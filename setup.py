__author__ = 'Aziz Alto'

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Mapping part-whole relations patterns to standard OWL-DL ALE+ document',
    'author': 'Aziz Alto',
    'url': 'iamaziz.github.com',
    'download_url': 'None',
    'author_email': 'iamaziz.alto@gmail.com',
    'version': '0.1',
    'install_requires': ['lxml'],
    'packages': ['pyowl', 'pyowl.support'],
    'scripts': [],
    'name': 'pyowl'
}

setup(**config)
