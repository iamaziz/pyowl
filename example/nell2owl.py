
import pandas as pd

csvfile = "dataset-nell/bkisiel_aaai10_08m.100.SSFeedback.csv"     # sample 1, iteration 100 (BEST SAMPLE)
s1 = pd.read_csv(csvfile, sep='\t*', usecols=['Relation', 'Action', 'Entity', 'Value', 'Probability']) # best sample

print s1[s1.Relation == "competeswith"][:1000]