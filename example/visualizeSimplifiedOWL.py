import sys

sys.path.append("../../pyowl/")
from graphowl import *
from mapper import *


#___ simplified OWL file 
input_onto = "dataset-simple/human-simulated.owl"

#___ transform
t = TransformOWL(input_onto)
onto_dict, onto_path = t.iterate_ontology(DEBUG_FILE=0)

#____ visualize onto from the dict generated after parsing the simplified OWL
g = OWLGraph()
g.graph_it(onto_dict, output_image='test', open_image=True, orientation="BT")
# print onto_dict