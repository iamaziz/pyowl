import sys

sys.path.append("../../pyowl/")
from graphowl import *
from mapper import *


simple_owl = "dataset-simple/simplified-person.owl"
#__ transform
t = TransformOWL(simple_owl)
onto_dict, onto_path = t.iterate_ontology(KEY_WORDS=['has-a', 'marriedTo'])

#__ visualize
g = OWLGraph()
g.graph_it(onto_dict, 'test-graph', orientation="BT")
