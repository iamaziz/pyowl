import sys

sys.path.append("../../pyowl/")
# from mapper import *
# from graphowl import *
from pyowl import mapper
from pyowl import graphowl
from pyowl.support import dict2owl
from pyowl.support import kb2dict


#____ read input knowledge bases


#== input as natural language statements
slist = ["Apache is-a Server","Tomcat is-a Server", "WebBase partOf Server", "Extension partOf Server", "Extension attachedTo Browser"]
statements = kb2dict.statement_dict(slist)

#== input as a csv file
csvfile = kb2dict.kb_dict("dataset/kb-wiki-paper.csv") # kb-body.csv, kb-person.csv, kb-server.csv, kb-wiki-paper.csv


## choose which onto dict to use
onto = csvfile

#____ visualize onto from the input dict
# g = graphowl.OWLGraph()
# g.graph_it(onto, 'test-graph', orientation="BT")

#____ generate simplified OWL syntax from the input onto dict
keywords, simple_owl = dict2owl.main(onto)

#____ transform the simplified syntax into standard OWL-DL ontology
mapper = mapper.TransformOWL(simple_owl)
onto_dict, onto_path = mapper.iterate_ontology(KEY_WORDS=keywords)
# print onto_dict

#____ visualize onto from the dict generated after parsing the simplified OWL
g = graphowl.OWLGraph()
g.graph_it(onto_dict, 'test-graph', orientation="BT")














#____ sample ontologies

def get_dict():

    #== input as kb dictionary

    anatomy_body_human = {'Arm': [('Hand', 'partOf')],
     'Body': [('Person', 'has'), ('Arm', 'partOf'), ('Head', 'partOf')],
     'Child': [('Father', 'has'), ('Mother', 'has')],
     'Circulatory_System': [('Heart', 'partOf')],
     'Ear': [],
     'Eye': [],
     'Father': [],
     'Hand': [('Arm', 'hasPart')],
     'Head': [('Ear', 'partOf'), ('Eye', 'partOf')],
     'Heart': [],
     'Husband': [('Father', 'is-a'), ('Wife', 'marriedTo')],
     'Man': [],
     'Mother': [],
     'Muscular_Organ': [('Heart', 'is-a')],
     'Organ': [('Body', 'composedOf'), ('Muscular_Organ', 'is-a')],
     'Person': [('Man', 'is-a'),
      ('Woman', 'is-a'),
      ('Child', 'is-a'),
      ('Husband', 'is-a'),
      ('Wife', 'is-a')],
     'Wife': [('Mother', 'is-a'), ('Husband', 'marriedTo')],
     'Woman': []}

    server = {
                'Server': [('Apache', 'is-a'), ('Tomcat', 'is-a'), ('Extension', 'partOf'), ('WebBase', 'partOf')],
                'Browser': [('Extension', 'attachedTo')],
                'Tomcat': [],
                'Extension': [],
                'Apache': [],
                'WebBase': [],
                }

    human_body = {

        'Body': [('Head', 'partOf'),('Arm', 'partOf')],
        'Arm' : [('Hand', 'partOf')],
        'Head': [('Eye', 'partOf'), ('Ear', 'partOf')],
        'Hand': [],
        'Eye' : [],
        'Ear' : [],
    }

    family = {
                'Person': [('Man', 'is-a'), ('Woman', 'is-a'), ('Child', 'is-a')],
                'Man': [('Husband', 'is-a'), ('Father', 'is-a')],
                'Woman': [('Wife', 'is-a'),('Mother', 'is-a')],
                'Husband': [('Father', 'is-a'), ('Wife', 'married-to')],
                'Wife': [('Mother', 'is-a'), ('Husband', 'married-to')],
                'Father': [],
                'Mother': [],
                'Child': [('Father', 'has-a'), ('Mother', 'has-a')]
                }
    return anatomy_body_human
