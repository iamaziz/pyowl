__author__ = "A.Aziz Altowayan"
__email__ = "aa10212w@pace.edu"
__version__ = "0.1"
# Sun Nov  2 12:40:48 EST 2014

# For the complete attributes list of pydot:
# http://www.graphviz.org/doc/info/attrs.html

class OWLGraph(object):
    """docstring for OWLGraph, Vizualize OWL ontology."""

    def __init__(self):
        pass

    @staticmethod
    def graph_it(onto_dict, output_image="test-graph", open_image = True, orientation="BT",):
        """
        input:
            onto: ontology dictionary, the returned dict of TransformOWL.iterate_ontology()
            file_name: the output image name
            open_image: optional boolean to open image after saving it.
            orientation: of the graph ["TB", "LR", "BT", "RL"] TobBottom, LeftRight, BottomTop, RightLeft
        return:
            generate onto graph image and save it into ./graph/output_image.png
            name (path) of the generated figure
        """

        import pydot

        graph = pydot.Dot(graph_type='digraph', rankdir=orientation) # g_type=['graph', digraph],

        onto_dict = {key: value for key, value in onto_dict.items() if key is not None}  # remove None in classes
        # remove None in childs i.e. {'parent': [(None, 'is-a')]}
        clean = {}
        for k, v in onto_dict.items():
            clean.setdefault(k, [])
            for c, r in v:
                if c is not None and r is not None:
                    clean[k].append((c, r))
        onto_dict = clean

        graphed = []
        # graph the childern concepts
        for axiom in onto_dict.items():
            concept, childs = axiom
            if len(childs) > 0:
                for c in childs:
                    child, relation = c
                    src = pydot.Node(child)
                    dst = pydot.Node(concept)
                    edge = pydot.Edge(src, dst, label=relation)
                    graph.add_edge(edge)
                    # record graphed to remove from dictionary
                    graphed.append(child)

        # # graph the remaining onto concepts excluding the graphed ones in the previous iteration
        # onto_dict = {key: value for key, value in onto_dict.items() if key not in graphed}
        # for axiom in onto_dict.items():
        #     concept, nothing = axiom
        #     src = pydot.Node(concept)
        #     dst = pydot.Node("Thing")
        #     edge = pydot.Edge(src, dst, label='is-a')
        #     graph.add_edge(edge)

        # create output dir and save graph
        import os

        fig_dir = "pyowl-output/png/"
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)

        fig_name = fig_dir + output_image
        if not fig_name.lower().endswith('.png'):
            fig_name += ".png"

        # plot and save png
        graph.write_png(fig_name)

        # open png
        if open_image:
            os.system('open {}'.format(fig_name))

        return fig_name

if __name__ == "__main__":

    # from mapper import TransformOWL

    # transfomer = TransformOWL("/Users/Aziz/Dropbox/DeVelop/owl-PyMapper/pyowl/output/human-simulated-final.owl")
    # {'Body': [('Human', 'composedOf'), ('Head', 'partOf'), ('Arm', 'partOf'), ('composedOf_Body', 'composedOf'), ('partOf_Body', 'partOf'), ('partOf_Body', 'partOf')], 'Head': [('Human', 'hasPart'), ('hasPart_Head', 'hasPart')], 'Human': [('Male', 'is-a'), ('Female', 'is-a')], 'partOf_Body': [], 'composedOf_Body': [], 'Female': [], 'Male': [], 'hasPart_Head': [], 'Arm': []}
    # transfomer = TransformOWL("/Users/Aziz/Dropbox/DeVelop/owl-PyMapper/pyowl/output/human-simulated-final-mapping.owl")

    # transfomer = TransformOWL("/Users/Aziz/Dropbox/DeVelop/owl-PyMapper/pyowl/data/human-simulated.owl")
    # onto_dict, fpath = transfomer.iterate_ontology()

    # print onto_dict
    # print fpath

    # Or
    onto_dict = {'Tomcat': [], 'Extension': [], 'Server': [('Apache', 'is-a'), ('Tomcat', 'is-a'), ('Extension', 'partOf'), ('WebBase', 'partOf')], 'Apache': [], 'WebBase': [], 'Browser': [('Extension', 'attachedTo')]}
    g = OWLGraph()
    g.graph_it(onto_dict, output_image="graphowl-testing")

