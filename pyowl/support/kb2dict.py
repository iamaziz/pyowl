import csv

def kb_dict(csv_name, delimiter=' '):
    """
    Read knoweldge bases (sentences) from a csv file and return a dictionary.

    input: the csv file name for the knowledge base dataset.
    return: a dictionary of the input knowledge bases.

    hint:
        for kb format from different knowledge extraction dataset i.e. NELL's `DataFrame` and `.csv` files. 
    """

    onto_dict = {}
    with open(csv_name, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=delimiter)
        for row in spamreader:
            statement = ' '.join(row)
            e1, r, e2 = statement.split(" ")
            onto_dict.setdefault(e1, [])
            onto_dict.setdefault(e2, [])
            onto_dict[e2].append((e1,r))

    return onto_dict

def statement_dict(statements):
    """
    Read knoweldge bases (sentences) from a list and return a dictionary.

    input: a list of knoweldge base statements i.e. ['Head partOf Body', 'Eye partOf Head'].
    return: a dictionary of the input knowledge bases.

    hint:
        for kb input from natural language sentences.
    """

    onto_dict = {}
    for kb in statements:
        e1, r, e2 = kb.split(" ")
        onto_dict.setdefault(e1, [])
        onto_dict.setdefault(e2, [])
        onto_dict[e2].append((e1,r))
    return onto_dict


if __name__ == "__main__":
    # test
    # from statemetns list
    statements = ["Arm partOf Body", "Hand partOf Arm", "Head partOf Body", "Ear partOf Head", "Eye partOf Head"]
    print statement_dict(statements)

    # from csv file
    print kb_dict("../example/dataset/kb-server.csv")
    
