
import sys
import os
sys.path.append("../../pyowl/")
# from owltree import *
from pyowl import owltree

def main(onto_dict):
    """
    input: ontology dictionary (concepts and relations)
    action: generate the simplified OWL representation
    return: 
        1) keywords: list of the relationships names.
        2) real path of the auto-generated OWL file.
    """
    # # generating a new ontolgoy from an ontology dictionary.
    input_onto = onto_dict

    # tree to generate ontology classes
    # tree = GenerateTree()
    tree = owltree.GenerateTree()

    # output dir
    out_dir = 'pyowl-output/auto_generated'
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    # output file
    fout = 'simulated-auto-ontology.owl'
    auto_onto = open(os.path.join(out_dir, fout), 'w')


    # write the ontolgoy header
    fname, contents = tree.create_onto_header()
    f = open(fname, 'r')
    for line in f.readlines():
        line = line.strip()
        auto_onto.write(line)
        auto_onto.write('\n')
    f.close()
    os.system("rm {}".format(f.name)) # delete the temp header file

    # generate simplified OWL axioms and write them to file
    keywords = []
    for axiom in input_onto.items():
        # concept, childs = axiom
        concept, relatives = axiom
        
        # multi-relation
        if len(relatives) > 1:
            for c in relatives:
                relation, child  = c
                if not relation == "is-a":
                    keywords.append(relation)
                # generate owl element
                # filename = tree.create_simplified_class(child, relations=[(concept, relation)], sysout=0)
                filename = tree.create_simplified_class(concept, relations=[(child, relation)], sysout=0)
                f = open(filename, 'r')
                for line in f.readlines():
                    line = line.strip()
                    auto_onto.write(line)
                    auto_onto.write('\n')
                    
        # bi-relation
        elif len(relatives) == 1:
            relation, child = relatives[0]
            keywords.append(relation)
            # generate owl element
            # filename = tree.create_simplified_class(child, relations=[(concept, relation)], sysout=0)
            filename = tree.create_simplified_class(concept, relations=[(child, relation)], sysout=0)
            f = open(filename, 'r')
            for line in f.readlines():
                line = line.strip()
                auto_onto.write(line)
                auto_onto.write('\n')
        # uni-relation
        else:
            # generate owl element
            filename = tree.create_simplified_class(concept, sysout=0)
            f = open(filename, 'r')
            for line in f.readlines():
                line = line.strip()
                auto_onto.write(line)
                auto_onto.write('\n')
        
        auto_onto.write('\n\n')

    # (clean up) delete the temp_file
    f.close()
    os.system("rm {}".format(f.name))
                
                
    # write the closing tag
    auto_onto.write("</rdf:RDF>")
    auto_onto.close()
    print("Generated simulated OWL at: {}\n".format(auto_onto.name))

    # relations keywords i.e. partOf, has-a, ..
    keywords = list(set(keywords))
    
    fpath = os.path.realpath(auto_onto.name)

    return keywords, fpath


if __name__ == '__main__':
    # sample knowledge base dictionary
    server = {
                'Tomcat': [], 
                'Extension': [], 
                'Server': [('Apache', 'is-a'), ('Tomcat', 'is-a'), ('Extension', 'partOf'), ('WebBase', 'partOf')], 
                'Apache': [], 
                'WebBase': [], 
                'Browser': [('Extension', 'attachedTo')]}

    family = {
                'Person': [('Man', 'is-a'), ('Woman', 'is-a'), ('Child', 'is-a')],
                'Man': [('Husband', 'is-a'), ('Father', 'is-a')],
                'Woman': [('Wife', 'is-a'),('Mother', 'is-a')],
                'Husband': [('Father', 'is-a'), ('Wife', 'married-to')],
                'Wife': [('Mother', 'is-a'), ('Husband', 'married-to')],
                'Father': [],
                'Mother': [],
                'Child': [('Father', 'has-a'), ('Mother', 'has-a')]
                }
    
    # Generate simulated syntax from onto dict
    keywords, onto_path = main(family)
    print onto_path

    # Transform to standard syntax
    # mapper = TransformOWL('auto_generate/auto-simulated-human.owl')
    # mapper = TransformOWL('output-data/auto-simulated-human-mapping.owl') # bug with server
    # onto_dict, onto_path = mapper.iterate_ontology(KEY_WORDS=keywords)
    # print onto_dict

    # visualize the onto dict
    # g = OWLGraph()
    # g.graph_it(onto_dict, 'test-graph', orientation="BT")



