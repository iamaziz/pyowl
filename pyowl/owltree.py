__author__ = "A.Aziz Altowayan"
__email__ = "aa10212w@pace.edu"
__version__ = "0.3"
# Sun Nov  2 12:40:48 EST 2014

class GenerateTree(object):
    import lxml.etree
    import sys
    import os

    def __init__(self, etree=lxml.etree, system=sys):
        self.et = etree
        self.sys = system

        # namespaces maps
        self.OWL_NS = "http://www.w3.org/2002/07/owl#"
        self.RDFS_NS = "http://www.w3.org/2000/01/rdf-schema#"
        self.RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        self.RELATION = "http://test.com/aziz/relation#"

        self.NS_MAP = {
            "owl": self.OWL_NS, "rdfs": self.RDFS_NS, "rdf": self.RDF_NS, "relation": self.RELATION
        }

    ##TODO: TAKE OUT TO a separate class for simplified OWL and Semi-Auto Ontology building
    def create_simplified_class(self, classname, onto_ns="relation", relations=[], subclass_type=[], sysout=0, DEBUG_FILE=0):
        """
        subclass_types: a list of relation names (keywords) to be considered as is-a relations
        return: a file name of the created OWL object.
        """

        subclass_types = ["is-a", "generalizations"]  # types of relationships property that are treated as a `subClassOf`

        if subclass_type:
            [subclass_types.append(subtype) for subtype in subclass_type]

        root = self.et.Element('AUX_Class', nsmap=self.NS_MAP)
        sheet = self.et.ElementTree(root)
        # add main element
        subclass = self.et.SubElement(root, self.et.QName(self.OWL_NS, "Class"),
                                      about="{0}#{1}".format(onto_ns, classname))
        if relations:
            for r_entity, r_property in relations:
                if r_property in subclass_types:  # i.e. "is-a", "generalizations"
                    self.et.SubElement(subclass, self.et.QName(self.RDFS_NS, "subClassOf"), resource="{0}#{1}".format(onto_ns, r_entity))
                else:
                    self.et.SubElement(subclass, self.et.QName(self.RELATION, r_property), resource="{0}#{1}".format(onto_ns, r_entity))

        if sysout:
            sheet.write(self.sys.stdout, pretty_print=True)
        if DEBUG_FILE:
            sheet.write("debug_file.owl", pretty_print=True)

        sheet.write("temp_file.owl", pretty_print=True)
        temp_file_path = self.os.path.realpath("temp_file.owl")
        self.clean_up(temp_file_path)

        return temp_file_path


    def clean_up(self, file_path, replacement=[]):
        """
            Read the generated intermediate file line-by-line, and:
                1) remove lines start/end with '<AUX_' / '</AUX_'
                2) add rdf: to resource= and about=
        """
        from tempfile import mkstemp
        from shutil import move
        import os

        if not replacement:
            replacement = ["<!-- Auto-generated Element -->", "<!-- End of auto-generated -->"]

        # Create temp file
        fh, abs_path = mkstemp()
        new_file = open(abs_path, 'w')
        old_file = open(file_path)
        for line in old_file:
            if line.startswith("<AUX_"):
                new_file.write(replacement[0] + '\n')
            elif line.startswith("</AUX_"):
                new_file.write(replacement[1] + '\n')
            else:
                # add rdf: to resource= and about=
                if " resource=\"" in line:
                    line = "rdf:resource=".join(line.rsplit("resource=", 1))
                if " about=" in line:
                    line = "rdf:about=".join(line.rsplit("about=", 1))
                # write out line
                new_file.write(line)

        #close temp file
        new_file.close()
        os.close(fh)
        old_file.close()
        #Remove original file
        os.remove(file_path)
        #Move new file
        move(abs_path, file_path)

    def create_onto_header(self):
        """
        return: 1) the generated header_file_path, and 2) header contents in a var
        """
        header = """<?xml version="1.0"?>

        <!DOCTYPE rdf:RDF [
        <!ENTITY owl "http://www.w3.org/2002/07/owl#" >
            <!ENTITY xsd "http://www.w3.org/2001/XMLSchema#" >
            <!ENTITY rdfs "http://www.w3.org/2000/01/rdf-schema#" >
            <!ENTITY relation "http://www.w3.org/2001/sw/BestPractices/OEP/SimplePartWhole/part.owl#" >
            <!ENTITY rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#" >
        ]>
        <rdf:RDF xmlns="http://www.semanticweb.org/aziz/pyowl-onto#"
        xml:base="http://www.semanticweb.org/aziz/pyowl-onto"
        xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
        xmlns:owl="http://www.w3.org/2002/07/owl#"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:relation="http://www.w3.org/2001/sw/BestPractices/OEP/SimplePartWhole/part.owl#">
        <owl:Ontology rdf:about="http://www.semanticweb.org/aziz/pyowl-onto"/>
        """
        # xmlns:relation="http://test.com/aziz/relation#">
        head_file = open("header.owl", "w")
        head_file.write(header)
        head_file.close()
        # self.os.system("rm {}".format(head_file.name))

        head_file_path = self.os.path.realpath(head_file.name)

        return head_file_path, header

    ##TODO: END of .... TAKE OUT TO a separate class for Semi-Auto Ontology building

    def create_prop_subclass(self, prop="", parent_class="", onto_ns='', output_file=0, cardinality="some"):
        """
        generate the mapping:

        <owl:Class rdf:about="onto#WebBase">
        <!-- Auto generated mapping -->
          <rdfs:subClassOf>
            <owl:Restriction>
              <owl:onProperty rdf:resource="onto#partOf"/>
              <owl:someValuesFrom rdf:resource="onto#Server"/>
            </owl:Restriction>
          </rdfs:subClassOf>
        <!-- End mapping -->

        cardinality: [optional] with two values `some (there exists)` or `all (for all)`
        """
        root = self.et.Element('AUX_Prop_SubClass', nsmap=self.NS_MAP)
        sheet = self.et.ElementTree(root)
        # add elements
        subclass = self.et.SubElement(root, self.et.QName(self.RDFS_NS, "subClassOf"))

        # can be moved to one function: that takes 1) property name 2) parent
        # class 3) ns name 4) cardinality
        restriction = self.et.SubElement(subclass, self.et.QName(self.OWL_NS, "Restriction"))
        onProperty = self.et.SubElement(restriction, self.et.QName(self.OWL_NS, "onProperty"),
                                        resource="{0}#{1}".format(onto_ns, prop))

        cardinality += "ValuesFrom"
        valuesFrom = self.et.SubElement(restriction, self.et.QName(self.OWL_NS, cardinality),
                                        resource="{0}#{1}".format(onto_ns, parent_class))
        # END of create the function.

        # print/write to file
        if output_file is not 0:
            sheet.write(output_file, pretty_print=True)
        else:
            sheet.write(self.sys.stdout, pretty_print=True)

        return sheet  # not used

    def create_aux_class(self, prop="", parent_class="", onto_ns="", output_file=0, cardinality="all"):
        """
        generate a mapping like:

        <!-- Auxiliary Class -->
        <!-- Auto generated mapping -->
          <owl:Class rdf:about="onto#AGGREGATE_CLASS">
            <owl:equivalentClass>
              <owl:Restriction>
                <owl:onProperty rdf:resource="ONTO_NS#PROP"/>
                <owl:CARDINALITYValuesFrom rdf:resource="ONTO_NS#PARENT_CLASS"/>
              </owl:Restriction>
            </owl:equivalentClass>
          </owl:Class>
        <!-- End mapping -->
        """

        aggregate_class = prop + '_' + parent_class

        root = self.et.Element('AUX_CLASS', nsmap=self.NS_MAP)
        sheet = self.et.ElementTree(root)

        top = self.et.SubElement(root, self.et.QName(self.OWL_NS, "Class"), about="{0}#{1}".format(onto_ns, aggregate_class))
        equivalent = self.et.SubElement(top, self.et.QName(self.OWL_NS, "equivalentClass"))
        # can be moved to one function: that takes 1) property name 2) parent
        # class 3) ns name 4) cardinality
        restriction = self.et.SubElement(equivalent, self.et.QName(self.OWL_NS, "Restriction"))
        onProperty = self.et.SubElement(restriction, self.et.QName(self.OWL_NS, "onProperty"),
                                        resource="{0}#{1}".format(onto_ns, prop))

        cardinality += "ValuesFrom"
        valuesFrom = self.et.SubElement(restriction, self.et.QName(self.OWL_NS, cardinality),
                                        resource="{0}#{1}".format(onto_ns, parent_class))
        # end of the function

        if output_file is not 0:
            output_file.write("<!-- Auxiliary Class -->\n")
            sheet.write(output_file, pretty_print=True)
            output_file.write("\n\n")
        else:
            sheet.write(self.sys.stdout, pretty_print=True)

        return sheet  # not used

    def create_aux_prop(self, prop="", onto_ns="", output_file=0, transitive=1):

        root = self.et.Element('AUX_Prop', nsmap=self.NS_MAP)
        sheet = self.et.ElementTree(root)

        objectProperty = self.et.SubElement(root, self.et.QName(self.OWL_NS, "ObjectProperty"), about="{0}#{1}".format(onto_ns, prop))

        if transitive == 1:
            transitivity = self.et.SubElement(objectProperty, self.et.QName(self.RDF_NS, "type"),
                                              resource="&owl;TransitiveProperty")

        if output_file is not 0:
            output_file.write("<!-- Property -->\n")
            output_file.write(self.et.tostring(root, pretty_print=True).replace('&amp;', '&'))  # Temporary Solution
            # sheet.write(output_file, pretty_print=True)
            output_file.write("\n\n")
        else:
            sheet.write(self.sys.stdout, pretty_print=True)

        return sheet, root

    
    def create_class(self, prop="", whole="", part="", onto_ns='', output_file=0, cardinality="some"):
            """
            generate the mapping:
            <!-- Auto generated mapping -->
            <owl:Class rdf:about="onto#WebBase">
              <rdfs:subClassOf>
                <owl:Restriction>
                  <owl:onProperty rdf:resource="onto#partOf"/>
                  <owl:someValuesFrom rdf:resource="onto#Server"/>
                </owl:Restriction>
              </rdfs:subClassOf>
              </owl:Class>
            <!-- End mapping -->

            cardinality: [optional] with two values `some (there exists)` or `all (for all)`
            """
            root = self.et.Element('AUX_Prop_SubClass', nsmap=self.NS_MAP)
            sheet = self.et.ElementTree(root)
            # add elements
            class_ = self.et.SubElement(root, self.et.QName(self.OWL_NS, "Class"), about="{0}#{1}".format(onto_ns, whole)) # the only difference from create_prop_subclass()
            subclass = self.et.SubElement(class_, self.et.QName(self.RDFS_NS, "subClassOf"))

            # can be moved to one function: that takes 1) property name 2) parent
            # class 3) ns name 4) cardinality
            restriction = self.et.SubElement(subclass, self.et.QName(self.OWL_NS, "Restriction"))
            onProperty = self.et.SubElement(restriction, self.et.QName(self.OWL_NS, "onProperty"), resource="{0}#{1}".format(onto_ns, prop))

            cardinality += "ValuesFrom"
            valuesFrom = self.et.SubElement(restriction, self.et.QName(self.OWL_NS, cardinality), resource="{0}#{1}".format(onto_ns, part))
            # END of create the function.

            # print/write to file
            if output_file is not 0:
                sheet.write(output_file, pretty_print=True)
            else:
                sheet.write(self.sys.stdout, pretty_print=True)

            return sheet  # not used

if __name__ == "__main__":
    tree = GenerateTree()
    # uni-sample
    # tree.create_simplified_class("matrix", relations=[('ken', 'act-in')], sysout=1, DEBUG_FILE=1)

    # multi-sample
    # sample = onto_dict = {'Server': [('Apache', 'is-a'), ('Tomcat', 'is-a'), ('Extension', 'partOf'), ('WebBase', 'partOf')], 'Browser': [('Extension', 'attachedTo')]}
    # for axiom in sample.items():
    # concept, childs = axiom
    #     for c in childs:
    #         child, relation = c
    #         tree.create_simplified_class(concept, relations=[(child, relation)], sysout=1, DEBUG_FILE=1)



    # test create_aux_class()
    tree.create_aux_class(prop="nani", onto_ns="NS", parent_class="PARENT")
    tree.create_aux_prop("partOf", "ns")
    tree.create_prop_subclass("hasPart", "nani", "ns")