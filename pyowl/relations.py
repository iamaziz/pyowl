__author__ = "A.Aziz Altowayan"
__email__ = "aa10212w@pace.edu"
__version__ = "0.3"
# Sun Nov  2 12:40:48 EST 2014


class SupportedRelationTypes(object):
    """
        Manage, update, change, and view the supported relationships types.
    """

    def __init__(self):
        self.supported = ("<relation:partOf ", "<relation:attachedTo ", "<relation:typeOf ", "<relation:composedOf ", "<relation:hasPart ", "<relation:countrystates ")

    def add_type(self, new_type):
        """Add a new relationship type to the supported relationship tags. e.g.) partOf in <rdfs:partOf ... >"""

        t = "<rdfs:" + new_type + " "
        s = [x for x in self.supported]  # list of supported types
        if t not in s:
            self.supported = self.supported + (t,)
            print("New type: {} is added to supported KEYWORDS.".format(t))
        else:
            print("Type is already supported!")

    def show(self):
        print(self.supported)

    def types(self):
        return self.supported