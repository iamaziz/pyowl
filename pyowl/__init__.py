#!/usr/bin/env python

__author__ = 'Aziz'

from graphowl import *
from mapper import *
from pyowl.support import dict2owl, kb2dict
from pyowl.support import kb2dict


def in_statements():
    """KB input as a list of natural language statements"""
    slist = ["Apache is-a Server","Tomcat is-a Server", "WebBase partOf Server", "Extension partOf Server", "Extension attachedTo Browser"]
    statements = kb2dict.statement_dict(slist)


def read_kb_csv(csvfile):
    """Read kb input from a file with one KB statement per line, i.e. `Subject is-a Object`,
    return: the ontology representated in a dictionary data structure.
    """
    if csvfile:
        onto = kb2dict.kb_dict(csvfile)
    else:
        onto = kb2dict.kb_dict("example/dataset/kb-wiki-paper.csv") # kb-body.csv, kb-person.csv, kb-server.csv, kb-wiki-paper.csv
    return onto


def viz_onto(onto):
    """visualize onto from the input dict"""
    # argparse: -v 
    g = OWLGraph()
    # print onto 
    g.graph_it(onto, 'test-graph', orientation="BT")


def simple_owl(onto):
    """generate the simplified OWL syntax from the input ontology dictitionry"""
    # argparse: -s
    keywords, simple_owl = dict2owl.main(onto)
    return keywords, simple_owl


def transform_owl(simple_owl):
    """transform the simplified syntax `simple_owl` into standard OWL-DL ontology"""
    mapper = TransformOWL(simple_owl)
    onto_dict, onto_path = mapper.iterate_ontology(KEY_WORDS=keywords)
    # print onto_dict
    return onto_dict, onto_path


def viz_owl(onto_dict):
    """visualize onto from the dict generated after parsing the simplified OWL"""
    g = OWLGraph()
    g.graph_it(onto_dict, 'test-graph', orientation="BT")


def sample_data():
    #____ sample ontologies

    #== input as kb dictionary
    server = {
                'Server': [('Apache', 'is-a'), ('Tomcat', 'is-a'), ('Extension', 'partOf'), ('WebBase', 'partOf')],
                'Browser': [('Extension', 'attachedTo')],
                'Tomcat': [],
                'Extension': [],
                'Apache': [],
                'WebBase': [],
                }

    human_body = {

        'Body': [('Head', 'partOf'),('Arm', 'partOf')],
        'Arm' : [('Hand', 'partOf')],
        'Head': [('Eye', 'partOf'), ('Ear', 'partOf')],
        'Hand': [],
        'Eye' : [],
        'Ear' : [],
    }

    family = {
                'Person': [('Man', 'is-a'), ('Woman', 'is-a'), ('Child', 'is-a')],
                'Man': [('Husband', 'is-a'), ('Father', 'is-a')],
                'Woman': [('Wife', 'is-a'),('Mother', 'is-a')],
                'Husband': [('Father', 'is-a'), ('Wife', 'married-to')],
                'Wife': [('Mother', 'is-a'), ('Husband', 'married-to')],
                'Father': [],
                'Mother': [],
                'Child': [('Father', 'has-a'), ('Mother', 'has-a')]
                }


def main():
    import argparse
    #___ parse input
    parser = argparse.ArgumentParser('pyowl module')
    parser.add_argument("KB", help="Knowledge base file where the triple sentences are", type=str)
    parser.add_argument("-a", help="actions: [viz, simple, generate]", default='viz', type=str)
    # parser.add_argument("-t", help="", default='pdf', type=str)
    args = parser.parse_args()


    f = args.KB
    onto_dict = kb2dict.kb_dict(f)
    
    viz_onto(onto_dict)


if __name__ == '__main__':
    main()

