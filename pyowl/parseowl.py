__author__ = "A.Aziz Altowayan"
__email__ = "aa10212w@pace.edu"
__version__ = "0.3"
# Sun Nov  2 12:40:48 EST 2014


class ParseOWL(object):
    """docstring for ParseOWL"""

    import re

    def __init__(self):
        pass

    def get_class_name(self, element):
        """return: The class name (Server) of <owl:Class rdf:about="ns#Server"> element"""

        try:
            # ns_class = self.re.findall(r"rdf:about=\"(\D+)\"", element)[0]
            ns_class = self.re.findall(r"rdf:about=\"(\S+)\"", element)[0]  # S to match anything
            class_name = ns_class.split("#")[1]
            return class_name

        except:
            print("No class Match for: {}".format(element))
            return "NO-CLASS"

    def get_prop_name(self, element):
        """
        input: an RDFS element, and
        return: the property name.
        # e.g.) partOf in <rdfs:partOf rdf:resource="ns#Car"/>#
        e.g.) partOf in <relationship:partOf rdf:resource="ns#Car"/>
        """

        try:
            # regexp = self.re.compile(r"^.*<rdfs:([A-Za-z-_]*).*$", self.re.IGNORECASE)
            regexp = self.re.compile(r"^.*<relation:([A-Za-z-_]*).*$", self.re.IGNORECASE)
            # regexp = self.re.compile(r"^.*<relationship:([A-Za-z-_]*).*$", self.re.IGNORECASE)
            propName = regexp.sub(r"\1", element)
            # or maybe
            # propName = re.findall(r"<rdfs:(\D+\b)\s", element)[0]
            return propName
        except:
            print("Unexpected error:", self.sys.exc_info()[0])
            raise

    def get_parent_and_ns(self, element):
        """
        input: an RDFS element, and
        return:
        - Name of the aggregating class (parent)
            (i.e. Car in <rdfs:partOf rdf:resource="ns#Car"/>)
        - the namespace of the ontology.
            (i.e. ns in <rdfs:partOf rdf:resource="ns#Car"/>)
        """

        try:
            # resource = self.re.findall('rdf:resource=\"(\D+)\"/>', element)#[0]
            resource = self.re.findall('rdf:resource=\"(\S+)\"/>', element)  # [0] # S to match anything

            if len(resource) > 0:
                resource = resource[0]

            if resource and '#' in resource:

                ns = resource.split('#')[0]
                parent = resource.split('#')[1]

            elif '/' in resource:

                ns = resource.split('/')[:-1]
                parent = resource.split('/')[-1]
                """
                <owl:onProperty rdf:resource="&hrela;#hasDecisionMaker"/>
                <owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">2</owl:minCardinality>
                """
            else:
                parent, ns = None, None

            return parent, ns

        except:
            print("Unexpected error:", self.sys.exc_info()[0])
            raise

    def get_cardinality(self, element):

        # default is some for no input
        if "rdf:cardinal" in element:
            try:
                cardinal = self.re.findall("rdf:cardinal=\"(\D+)\"\s", element)[0]
            except:
                print("Expect cardinality value was not provided in the ontology(expects rdf:cardinal= )")
                print("Unexpected error:", self.sys.exc_info()[0])
                raise
        else:
            cardinal = "some"

        return cardinal

    def get_transitivity(self, element):

        if "rdf:transitive" in element:

            try:
                transitive = self.re.findall("rdf:transitive=\"(\D+)\"\s", element)[0]
                if transitive == "yes":
                    transitive = 1
                elif transitive == "no":
                    transitive = 0
            except:
                print("Unexpected error:", self.sys.exc_info()[0])
                raise
        else:
            transitive = 1

        return transitive


if __name__ == "__main__":
    pareser = ParseOWL()
    element = '<relation:winneringame transitive="yes" cardinality="some" rdf:resource="onto#n2002_stanley_cup"/>'
    
    print(pareser.get_class_name(element))
    print(pareser.get_prop_name(element))
    print(pareser.get_parent_and_ns(element))
    print(pareser.get_cardinality(element))
    print(pareser.get_transitivity(element))