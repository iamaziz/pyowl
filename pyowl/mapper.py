__author__ = "A.Aziz Altowayan"
__email__ = "aa10212w@pace.edu"
__version__ = "0.2"
# Sun Nov  2 12:40:48 EST 2014


class TransformOWL(object):

    """
    TransformOWL is the actual transformation class.
    input: file name (path) of the input OWL (simplified) ontology to be transformed.
    """
    import sys
    import re
    import os
    from parseowl import ParseOWL
    from relations import SupportedRelationTypes
    from owltree import GenerateTree

    def __init__(self, onto_file_name):

        output_path = "pyowl-output/standard-owl/"
        if not self.os.path.exists(output_path): self.os.makedirs(output_path)

        outfile = output_path + self.os.path.basename(onto_file_name)

        self.in_onto = onto_file_name
        self.out_onto = outfile.replace(".owl", "-formal.owl")

        # get supported keywords
        relations = self.SupportedRelationTypes()
        self.KEY_WORDS = relations.types()

    def get_keywords(self, keywords):
        """return:
            1. a tuple of relations KEYWORDS for elements to be transformed; if new keywords are introduced, they will be appended to built-in ones.
            2. end of onto file tag
            """
        if keywords:
            k = []
            for a in set(keywords):
                concat = "<relation:{} ".format(a)
                # concat = "<relationship:{} ".format(a)
                k.append(concat)
            kwords = tuple(k)
        else:
            kwords = self.KEY_WORDS

        eof_onto = "</rdf:RDF>"

        return kwords, eof_onto

    # Core Code (transformation and mapping)
    def iterate_ontology(self, KEY_WORDS=[], DEBUG_FILE=0, report=1, manual=True):
        """
        input:
            KEY_WORDS: a list of supported property names.
            report: boolean to print out each transformed Knowledge Base.
            DEBUG_FILE: boolean to remove (0) or keep (1) the intermediate transformed file
        action:
            Iterate the input ontology, and write out the output into the file `self.final_onto`
            if the input ontology a simplified (simulated) OWL format, it will be transformed into the standard OWL format.
        return:
            dict: A dictionary of the ontology relations from the input file, (used for generating the graph)
            string: absolute path of the output (mapping) ontology file

        Interfacing with owltree.GenerateTree(), parseowl.ParseOWL(), and relations.SupportedRelationTypes()

        iterate_ontolog() will iterate the input ontology file line by line, as it finds a line that starts with a KEYWORD, it will:
        1. remove that line and replace it with create_prop_subclass(), and store that 1. KEYWORD (basically a property name) with
        2. the name of the parent class, and 3. name space.
        """

        parser = self.ParseOWL()
        KEY_WORDS, EOF_ONTO = self.get_keywords(KEY_WORDS)


        tree = self.GenerateTree()

        in_onto = open(self.in_onto)
        out_onto = open(self.out_onto, "wb")

        class_list = []     # list of classes to be transformed
        onto_dict = {}      # ontology dictionary
        checked_prop = []   # list of relations check for transitivity
        inverse_class = []      # for inverse relation classes

        for i in xrange(0):
            in_onto.next()

        for line in in_onto: # another way of readlines() to support reading next() line
            line = line.strip()

            if not line.startswith(KEY_WORDS) and not line.startswith(EOF_ONTO):

                out_onto.write(line + '\n')

                # Adding Class and subClassOf concepts to onto_dict
                # get and store class name to onto_dict
                if line.startswith('<owl:Class'):
                    class_name = parser.get_class_name(line)
                    # print line
                    onto_dict.setdefault(class_name, [])

                # next subClassOf concept is a subclass of latest class name
                if line.startswith("<rdfs:subClassOf "):
                    parent, n = parser.get_parent_and_ns(line)
                    # if parent class is not yet in onto_dict(), create it
                    if parent not in onto_dict.keys():
                        onto_dict.setdefault(parent, [])
                    # onto_dict[parent].append((class_name, 'is-a'))
                    onto_dict[class_name].append(('is-a', parent))

                if line.startswith("<owl:onProperty "):
                    relation, n = parser.get_parent_and_ns(line)
                    next_line = in_onto.next().strip()      # look for <owl:..ValuesFrom .. >
                    parent, n = parser.get_parent_and_ns(next_line)

                    # if parent class is not yet in onto_dict(), create it
                    if parent not in onto_dict.keys():
                        onto_dict.setdefault(parent, [])
                    # update onto_dict
                    # onto_dict[parent].append((class_name, relation))
                    onto_dict[class_name].append((relation, parent))

                # in case <owl:[some|all]ValuesFrom .. > appears before <owl:onProperty ..>
                if line.startswith("<owl:someValuesFrom ") or line.startswith("<owl:allValuesFrom "):
                    parent, n = parser.get_parent_and_ns(line)
                    next_line = in_onto.next().strip()      # look for: <owl:onProperty ..>
                    relation, n = parser.get_parent_and_ns(next_line)

                    # if parent class is not yet in onto_dict(), create it
                    if parent not in onto_dict.keys():
                        onto_dict.setdefault(parent, [])
                    # update onto_dict
                    # onto_dict[parent].append((class_name, relation))
                    onto_dict[class_name].append((relation, parent))

            # The lines of transformation of the simplified approach starts here
            if line.startswith(KEY_WORDS):
            # if line.startswith("<rdfs:"): # to accommodate variant properties of auto-building

                prop_name                   = parser.get_prop_name(line)
                parent, ns                  = parser.get_parent_and_ns(line)


                # ask for relations restriction (transitive / existential)
                cardinality                 = parser.get_cardinality(line)
                # cardinality = input("What's the cardinality in '{} {} {}' only / some : ".format(class_name, prop_name, parent))
                # transitive                  = parser.get_transitivity(line)

                #TODO: move this to parseowl.get_transitivity()
                if manual:
	                if prop_name not in checked_prop:
	                    transitive = input("Is {} transitive 0/1: ".format(prop_name))
	                    checked_prop.append(prop_name)

                else:
                	transitive                  = parser.get_transitivity(line)


                # if parent class is not yet in onto_dict(), create it
                if parent not in onto_dict.keys():
                    onto_dict.setdefault(parent, [])

                
                # create inverse properties for partOf hasPart
                #TODO: find a better way to implement this
                def dummy_line(whole,part,prop, ns):
                	return '<relation:{} rdf:resource="{}#{}"/>'.format(prop,ns,part)

                if prop_name == 'partOf':
                    inv_prop = 'hasPart'
                    line = dummy_line(parent, class_name, inv_prop, ns)
                    inverse_class.append([inv_prop, parent, class_name, ns, out_onto, cardinality])
                    class_list.append([parent, inv_prop, class_name, ns, cardinality, transitive, line])


                
                # update onto_dict
                # onto_dict[parent].append((class_name, prop_name))
                onto_dict[class_name].append((prop_name, parent))

                # wrap the element with its info in the transformation queue
                class_list.append([class_name, prop_name, parent, ns, cardinality, transitive, line])

                # Convert the Property to a subClassOf the subsume class
                tree.create_prop_subclass(prop_name, parent, ns, out_onto, cardinality)

            # at the End Of onto File
            processed_prop = []
            if line.startswith(EOF_ONTO):
                """
                1) generate the aux_classes and their properties of the transformed elements
                2) append them to the generated-output file before closing up owl </rdf:RDF>
                """
                # create inverse classes
                if inverse_class:
                	for names in inverse_class:
                		inv_prop, parent, class_name, ns, out_onto, cardinality = names
                		tree.create_class(inv_prop, parent, class_name, ns, out_onto, cardinality)
                # class list is the classes
                if class_list:
                    for names in class_list:
                        class_name, prop_name, subsume_class, namespace, cardinality, transitive, element = names
                        # create additional property
                        if prop_name not in processed_prop: # avoid repeated properites
                        	tree.create_aux_prop(prop_name, namespace, out_onto, transitive)
                        	processed_prop.append(prop_name)
                        # create auxiliary class
                        # tree.create_aux_class(prop_name, subsume_class, namespace, out_onto, cardinality)

                        if report:
                            print("Transforming: {} ... with: \n\t\t\t\t... {} {} {}\n ".format(element, class_name, prop_name, subsume_class))

                out_onto.write(line + '\n')

        in_onto.close()
        out_onto.close()


        out_file_path = self.os.path.realpath(out_onto.name)

        if DEBUG_FILE:
            intermediate, debug = out_file_path, "{}-DEBUG.owl".format(out_file_path)
            self.os.system("cp {} {}".format(intermediate, debug))
            print("Debug file: {}".format(debug))

        # clean up the intermediate output
        owltree = self.GenerateTree()
        owltree.clean_up(out_file_path, replacement=["<!-- Auto generated mapping -->", "<!-- End mapping -->"])

        print("Input onto file: {} \nOutput onto file: {} \n\n".format(in_onto.name, out_file_path))

        return onto_dict, out_file_path  # final_onto_name

if __name__ == "__main__":
    # test = "/Users/Aziz/Dropbox/DeVelop/owl-PyMapper/pyowl/output/mini-simulated-mapping.owl"
    # simple_owl = "/Users/Aziz/Dropbox/DeVelop/owl-PyMapper/pyowl/data/simulated.owl"

    simple_owl = "/Users/Aziz/Desktop/pyowl/example/pyowl-output/auto_generated/simulated-auto-ontology.owl"

    mapper = TransformOWL(simple_owl)
    o_dict = mapper.iterate_ontology(DEBUG_FILE=0)
    print("Resulted onto_dict: {}".format(o_dict))

    from owltree import GenerateTree
    gtree = GenerateTree()


